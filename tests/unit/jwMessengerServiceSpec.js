'use strict';

describe("jwMessageService", function () {
    beforeEach(module("jwMessenger"));


    it("should return a blank array on init", inject(function (jwMessengerService) {
        expect(jwMessengerService.getMessages()).toEqual([]);
    }));

    it("should add a message to global queue", inject(function (jwMessengerService) {
        var message = jwMessengerService.addMessage({body: "hello"});;
        expect(jwMessengerService.getMessages().toString()).toBe(message.toString());
    }));

    it("should add a message to test queue with global only", inject(function (jwMessengerService) {
        var message = jwMessengerService.addMessage({body: "testMessage", id: "test"});
        expect(jwMessengerService.getMessages("test").toString()).toBe(message.toString());
        expect(jwMessengerService.getMessages().toString()).toBe(message.toString());
        expect(jwMessengerService.getMessages("test1")).toEqual([]);
    }));

    it("should remove a message from all queues using close on message", inject(function (jwMessengerService) {
        var message = jwMessengerService.addMessage({body: "testMessage", id: "test"});
        message.close();
        expect(jwMessengerService.getMessages("test")).toEqual([]);
        expect(jwMessengerService.getMessages()).toEqual([]);
    }));

    it("should remove a message from all queues using removeMessage", inject(function (jwMessengerService) {
        var message = jwMessengerService.addMessage({body: "testMessage", id: "test"});

        jwMessengerService.removeMessage(message);

        expect(jwMessengerService.getMessages("test")).toEqual([]);
        expect(jwMessengerService.getMessages()).toEqual([]);
    }));

    it("should remove ALL messages from ALL queues", inject(function (jwMessengerService) {

        jwMessengerService.addMessage({body: "testMessage", id: "test"});
        jwMessengerService.addMessage({body: "testMessage", id: "test2"});
        jwMessengerService.addMessage({body: "testMessage"});

        jwMessengerService.clearMessages();

        expect(jwMessengerService.getMessages("test")).toEqual([]);
        expect(jwMessengerService.getMessages("test2")).toEqual([]);
        expect(jwMessengerService.getMessages()).toEqual([]);
    }));

    it("should remove ALL test2 messages from queue", inject(function (jwMessengerService) {

        var test=jwMessengerService.addMessage({body: "testMes", id: "test"});
        var test2=jwMessengerService.addMessage({body: "testMessage1", id: "test2"});
        var test22=jwMessengerService.addMessage({body: "testMessage2", id: "test2"});
        var test23=jwMessengerService.addMessage({body: "testMessage3", id: "test2"});
        var test3= jwMessengerService.addMessage({body: "testMessage"});

        jwMessengerService.clearMessages("test2");

        expect(jwMessengerService.getMessages("test").toString()).toBe(test.toString());
        expect(jwMessengerService.getMessages("test2").toString()).toEqual("");
        expect(jwMessengerService.getMessages().toString()).toEqual([test,test3].toString());
    }));
});