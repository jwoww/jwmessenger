var messageApp = angular.module("MessageApp", ["ngRoute", "ngAnimate", "jwMessenger"])

messageApp.config(function ($routeProvider) {
    $routeProvider.when("/", {
        controller: "",
        templateUrl: "partials/welcome.html"
    }).when("/messageTest", {
            controller: "messageTestController",
            templateUrl: "partials/messageTest.html"
        }
    );
})
