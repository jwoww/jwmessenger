var controllers = {};
controllers.messageTestController = function ($scope, jwMessengerService) {
    $scope.message = {};
    $scope.addMessage = function () {
        jwMessengerService.addMessage({body:$scope.message.body, type:$scope.message.type,timer:$scope.message.timer,id:$scope.message.id});
        $scope.message = {};
    };
    $scope.clearMessages = function (id){
      jwMessengerService.clearMessages(id);
    };

    $scope.addTestMessages=function (){
        jwMessengerService.addMessage({body:"This is a test",type:"success",timer:2000});
        jwMessengerService.addMessage({body:"This is a second test",type:"info",timer:5000});

        jwMessengerService.addMessage({body:"Test box 1",id:"testBox1",type:"warning"});
        jwMessengerService.addMessage({body:"Test box 2",id:"testBox2",type:"danger"});
    }
}
messageApp.controller(controllers);