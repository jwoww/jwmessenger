angular.module("jwMessenger", ["ngAnimate"]);

angular.module("jwMessenger").factory("jwMessengerService", function ($timeout,$filter) {
    var service = {};

    var messages = {};
    messages.messageQueue = [];
    messages.messageTypes = ["success", "info", "danger", "warning"];
    messages.isMessageType = function (type) {
        if (this.messageTypes.indexOf(type) < 0) {
            return false;
        } else {
            return true
        }
    };
    messages.remove = function (message) {
        var messageIndex = this.messageQueue.indexOf(message);
        this.messageQueue.splice(messageIndex, 1);
        return true;
    };
    messages.clear = function (id) {
        if(angular.isDefined(id)){
            var items= $filter('filter')(this.messageQueue, {id:id});
            angular.forEach(items,function ($item){$item.close();})
        }else{
            this.messageQueue.splice(0, this.messageQueue.length)
        }
    }

    messages.getMessagesById = function (id) {
        return $filter('filter')(this.messageQueue, {id:id});
    }

    var mergeMessageSettings = function (message){
        var messageSettings = {
            body: "",
            type: "success",
            timer: 0,
            id: null
        };
        $().extend(messageSettings, message);
        return messageSettings;
    }

    service.messageService = messages;

    service.getMessages = function (id) {
        if (angular.isDefined(id)) {
           return this.messageService.getMessagesById(id);
        }
        return this.messageService.messageQueue;
    };

    service.removeMessage = function (message) {
        this.messageService.remove(message);
        return true;
    };
    service.clearMessages = function (id) {
        this.messageService.clear(id);
    }

    service.addMessage = function (message) {
        var messageService = messages;

        message = mergeMessageSettings(message);

        if (!messageService.isMessageType(message.type)) {
            throw "Invalid Type";
            return false;
        }
        message.close = (function () {
            service.removeMessage(this);
        });

        messageService.messageQueue.push(message);

        if (angular.isDefined(message.timer) && message.timer > 0) {
            $timeout(function () {
                return message.close()
            }, message.timer);
        }
        return message;
    };
    return service;
});

angular.module("jwMessenger").directive("jwMessenger", function () {
    return{
        scope: {
            locationID: '@locationId'
        },
        template: "<div class='messageContainer'>" +
            "<div ng-repeat='msg in getMessages()' class='alert alert-{{msg.type}} jwMessage'>" +
            "{{msg.body}}" +
            "<a ng-click='msg.close()'><div>close</div></a>" +
            "</div></div>",
        restrict: "E",
        controller: function ($scope, jwMessengerService) {
            $scope.getMessages = function () {
                  return jwMessengerService.getMessages($scope.locationID);
            }
        }
    }
});
